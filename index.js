const puppeteer = require("puppeteer");
const Logger = require("./utils");
require("dotenv").config();

if (!process.env.USERNAME) {
  Logger.error("username has not been defined");
  process.exit(1);
}
if (!process.env.PASSWORD) {
  Logger.error("password has not been defined");
  process.exit(1);
}
(async () => {
  const browser = await puppeteer.launch({
      headless: process.env.NODE_ENV !== "development"
  });
  const page = await browser.newPage();
  Logger.debug("navigating to page");
  await page.goto(
    "http://kairosnav.westeurope.cloudapp.azure.com:8081/Login.aspx"
  );
  Logger.debug("logging in");
  await page.type("input#ctl00_MainContent_UserName", process.env.USERNAME);
  await page.type("input#ctl00_MainContent_Password", process.env.PASSWORD);
  await page.click("input[name='ctl00$MainContent$ctl07']");
  await page.waitForNavigation();
  await page.click("a#ctl00_linkTimeSheets", {delay: 100});
  Logger.debug("selecting first row");
  await page.click("table#ctl00_MainContent_TableTimeSheet tr:nth-child(2)");
  await page.click("a#ctl00_MainContent_BEditTimeSheet", {delay: 500});
  await page.waitForNavigation();
  Logger.debug("filling fields");
  Logger.debug("  - type");
  await page.select(
    "select#ctl00_MainContent_HoursSheetTable_ctl02_DDLType",
    "2"
  );
  await page.waitForNavigation();
  Logger.debug("  - project");
  await page.select(
    "select#ctl00_MainContent_HoursSheetTable_ctl02_DDLJobNo",
    process.env.PROJECT_ID
  );
  await page.waitFor(1000);
  Logger.debug("  - kind");
  await page.select(
    "select#ctl00_MainContent_HoursSheetTable_ctl02_DDLJobTaskNo",
    "1100"
  );
  await page.waitFor(1000);
  Logger.debug("  - description");
  (await page.waitForSelector(
    "input#ctl00_MainContent_HoursSheetTable_ctl02_TbDesc"
  )).type(process.env.DESCRIPTION);
  await page.waitFor(750);
  const days = [...Array(5).keys()].map(
    e => `input#ctl00_MainContent_HoursSheetTable_ctl02_TbD${e + 1}`
  );
  for (let day in days) {
    Logger.debug(`  - day ${Number(day) + 1}`);
    await page.type(days[day], "8");
  }
  await page.click("input#ctl00_MainContent_HoursSheetTable_ctl02_chk");
  // await page.click("a#ctl00_MainContent_BSubmitTimeSheet");
  Logger.debug("done!");
  // await browser.close();
})().catch(err => {
  Logger.error(err);
  process.exit(1);
});
