const Winston = require('winston')
const {format} = Winston
const Logger = Winston.createLogger({
    transports: new Winston.transports.Console(),
    level: process.env.LOG_LEVEL || 'info',
    format: format.combine(
        format(info => {
            info.level = info.level.toUpperCase()
            return info
        })(),
        format.timestamp({
            format: 'YYYY-MM-DDTHH:mm:ss.SSSZZ'
        }),
        format.colorize({
            
        }),
        format.printf(info => `[${info.timestamp}] [${info.level}] ${info.message}`)
    )
})

module.exports = Logger
